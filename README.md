## Примеры запуска
Перед запуском приложения надо установить необходимые для работы приложения библиотеки. Это можно сделать следующими двумя способами:
* ```
  pip3 install -r requirements.txt
  ```
* ```
  pip3 install pdfrw
  pip3 install python-docx
  ```

### Notes
* Получение метаданных файла:
```shell
python3 main.py --file data/test.pdf --get-metadata
```

* Установление метаданных:
```shell
python3 main.py --file data/test.pdf --set-metadata author="НеДжон Алиен" created="01/04/2021 22:22:22"
```

* Если какие-то из метаданных не указаны, они заменяются на заранее предустановленные произвольные значения.


* Все ошибки записываются в `logs/journal.log`. Записывается время возникновения ошибки, файл, номер строки и сам текст ошибки. Примеры:
```log
2021-04-24 21:44:14,502 metadata_util.py: ERROR: __init__(): 36:	Unsupported file extension. Expected: "docx" or "pdf"; found: ".dads"
2021-04-24 21:46:18,075 metadata_util.py: ERROR: __init__(): 34:	File with name "test.dads" was not found or couldn't be opened.
2021-04-24 21:48:51,634 metadata_util.py: WARNING: get_metadata(): 61:	Creation time of file: "test.pdf" was <None> and was reset explicitly to the current time
2021-04-24 23:24:42,007 metadata_util.py: ERROR: __init__(): 64:	Error occurred while extracting metadata from file: "test.pdf": an integer is required (got type PdfString)
2021-04-25 00:57:12,021 main.py: ERROR: <module>(): 52:	Incorrect format of time provided. Format should be: 'DD/MM/YYYYY HH:MM:SS'. Example: 12/09/2018 12:12:21
2021-04-25 00:57:57,750 main.py: ERROR: <module>(): 42:	Incorrect number of arguments: 6, expected length is from 3 to 5.
2021-04-25 01:00:23,698 main.py: ERROR: <module>(): 55:	Unexpected token: "test_incorrect_token=fasdf", expected: "author=<Author>" and/or "created=<Creation time>"
2021-04-25 01:01:08,980 main.py: ERROR: <module>(): 64:	Unexpected token: "--new-metadata", expected: "--get-metadata" or "--set-metadata"
```
