import logging

import metadata_util
import sys
from datetime import datetime


def print_help():
    print('Incorrect usage of application. Usage:\n'
          '\t - python3 main.py --file <file name/path> --get-metadata\n'
          '\t - python3 main.py --file <file name/path> --set-metadata [author=<author name>] '
          '[<created=creation time in the following format: "%d/%m/%y %H:%M:%S">]')
    exit(0)


logging.basicConfig(filename='logs/journal.log', level=logging.DEBUG, format='%(asctime)s '
                                                                             '%(filename)s: '
                                                                             '%(levelname)s: '
                                                                             '%(funcName)s(): '
                                                                             '%(lineno)d:'
                                                                             '\t%(message)s')

args = sys.argv[1:]
if len(args) == 0 or len(args) == 2:
    print_help()

file = 'data/test.pdf'
if args[0] != '--file':
    print_help()
else:
    file = args[1]

reader = metadata_util.MetadataModifier(file)

if args[2] == '--get-metadata':
    reader.print_metadata()
elif args[2] == '--set-metadata':
    author = None
    created = None

    if len(args) < 3 or len(args) > 5:
        logging.error(f'Incorrect number of arguments: {len(args)}, expected length is from 3 to 5.')
        print_help()
    for i in range(3, len(args)):
        if args[i].startswith('author='):
            author = args[i][len('author='):]
        elif args[i].startswith('created='):
            try:
                print(args[i][len('created='):])
                created = datetime.strptime(args[i][len('created='):], '%d/%m/%Y %H:%M:%S')
            except ValueError:
                logging.error("Incorrect format of time provided. Format should be: 'DD/MM/YYYYY HH:MM:SS'. "
                              "Example: 12/09/2018 12:12:21")
        else:
            logging.error(f'Unexpected token: "{args[i]}", expected: "author=<Author>" and/or "created=<Creation time>"')
            print_help()

    reader.set_metadata(metadata_util.Metadata(author=author, created=created))

    print('Data was updated successfully!\n\n')
    print('=' * 15)
    reader.print_metadata()
else:
    logging.error(f'Unexpected token: "{args[2]}", expected: "--get-metadata" or "--set-metadata"')
    print_help()
