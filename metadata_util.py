import logging
import pathlib

from datetime import datetime

from docx import Document
from pdfrw import PdfReader, PdfWriter

logging.basicConfig(filename='logs/journal.log', level=logging.DEBUG, format='%(asctime)s '
                                                                             '%(filename)s: '
                                                                             '%(levelname)s: '
                                                                             '%(funcName)s(): '
                                                                             '%(lineno)d:'
                                                                             '\t%(message)s')


class Metadata:
    def __init__(self, author: str, created: datetime):
        self.author = author
        self.created = created


class MetadataModifier:
    def is_pdf(self):
        return self.extension == '.pdf'

    def is_docx(self):
        return self.extension == '.docx'

    @staticmethod
    def extract_datetime(pre_time):
        pre_time = pre_time[2:]

        year = pre_time[:4]
        pre_time = pre_time[4:]

        month = pre_time[:2]
        pre_time = pre_time[2:]

        day = pre_time[:2]
        pre_time = pre_time[2:]

        hours = pre_time[:2]
        pre_time = pre_time[2:]

        minutes = pre_time[:2]
        pre_time = pre_time[2:]

        seconds = pre_time[:2]
        pre_time = pre_time[3:]

        nanoseconds = pre_time.replace("'", '')

        time = f'{year}-{month}-{day}T{hours}:{minutes}:{seconds}+{nanoseconds}'

        return datetime.strptime(time, "%Y-%m-%dT%H:%M:%S%z")

    @staticmethod
    def format_datetime(time):
        year, month, day = str(time.year), str(time.month), str(time.day)
        hours, minutes, seconds = str(time.hour), str(time.minute), str(time.second)

        year = year.rjust(4, '0')
        month = month.rjust(2, '0')
        day = day.rjust(2, '0')

        hours = hours.rjust(2, '0')
        minutes = minutes.rjust(2, '0')
        seconds = seconds.rjust(2, '0')

        return f'D:{year}{month}{day}{hours}{minutes}{seconds}Z00\'00\''

    def __init__(self, file):
        self.file = file

        self.extension = pathlib.Path(self.file).suffix

        try:
            file = open(file, 'r')
        except FileNotFoundError:
            logging.error(f'File with name "{file}" was not found or couldn\'t be opened.')
            exit(0)

        if self.is_pdf():
            self.reader = PdfReader(self.file)
        elif self.is_docx():
            self.reader = Document(self.file)
        else:
            logging.error(f'Unsupported file extension. Expected: "docx" or "pdf"; found: "{self.extension}"')
            exit(0)

        created = None
        author = 'Muhammadjon Hakimov'

        try:
            if self.is_pdf():
                author = self.reader.Info.Creator.to_unicode()
                created = self.extract_datetime(self.reader.Info.CreationDate.to_unicode())
            elif self.is_docx():
                author = self.reader.core_properties.author
                created = self.reader.core_properties.created
            else:
                logging.error(f'This shouldn\'t happen: Unsupported file extension.'
                              f' Expected: "docx" or "pdf"; found: "{self.extension}"')
                exit(0)
        except Exception as e:
            logging.error(f'Error occurred while extracting metadata from file: "{self.file}": {e}')
            exit(0)

        if created is None:
            logging.warning(f'Creation time of file: "{self.file}" '
                            f'was <None> and was reset explicitly to the current time')
            created = datetime.now()

        self.created = created
        self.author = author

    def get_metadata(self):
        return Metadata(self.author, self.created)

    def print_metadata(self):
        print()

        print(f'Author: {self.author}')
        print(f'Created at: {self.created}')

    def set_metadata(self, metadata: Metadata):
        author = 'Muhammadjon Hakimov'
        created = datetime.now()

        if self.is_pdf():
            info = self.reader.Info
            if metadata.author is not None:
                author = metadata.author
            else:
                author = 'John Backham Hakimov'

            if metadata.created is not None:
                created = metadata.created
            else:
                created = datetime.now()

            info.Creator = author
            info.CreationDate = self.format_datetime(created)

            PdfWriter(self.file, trailer=self.reader).write()
        elif self.is_docx():
            info = self.reader.core_properties
            if metadata.author is not None:
                author = metadata.author
            else:
                author = 'Dark Voldemort Side'

            if metadata.created is not None:
                created = metadata.created
            else:
                created = datetime.now()

            info.author = author
            info.created = created
            self.reader.save(self.file)
        else:
            logging.error(f'This shouldn\'t happen: Unsupported file extension.'
                          f' Expected: "docx" or "pdf"; found: "{self.extension}"')
            exit(0)

        self.author = author
        self.created = created
